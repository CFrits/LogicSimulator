/**
 * Copyright: (c) 2019 Frits Wiersma
 * License: MIT
 */

define([
	'editor/Component',
	'editor/ComponentProperties',
	'editor/displayComponent',
	'shared/lib/extend'
], function (Component, ComponentProperties, displayComponent, extend) {
	function BufferComponentOE() {
		Component.call(this);

		this.pins = null;

		this.$container = null;
		this.$rect = null;

		this.properties = new ComponentProperties([]);

		this.layout();
	}

	extend(BufferComponentOE, Component);

	BufferComponentOE.prototype._save = function (data) {};

	BufferComponentOE.prototype._load = function (data) {};

	BufferComponentOE.prototype.layout = function () {
		var layout = displayComponent.layout(['OE',''], ['']);
		this.width = layout.width;
		this.height = layout.height;
		this.pins = layout.pins;
	};

	BufferComponentOE.prototype._display = function ($c) {
		this.$container = $c;
		this._updateDisplay();
	};

	BufferComponentOE.prototype._updateDisplay = function () {
		this.$container.innerHTML = '';
		this.$rect = displayComponent(this.$container, this.width, this.height, this.pins, '1');
		this.$rect.addEventListener('mousedown', this.mousedownCallback);

		if(this.selected) {
			this._select();
		}
	};

	BufferComponentOE.prototype._select = function () {
		this.$rect.setAttribute('stroke', '#0288d1');
	};

	BufferComponentOE.prototype._deselect = function () {
		this.$rect.setAttribute('stroke', '#000');
	};

	BufferComponentOE.prototype._serializeForSimulation = function () {
		return {
			name: 'bufferOE'
		};
	};

	BufferComponentOE.typeName = 'buffer';
	BufferComponentOE.sidebarEntry = {
		name: 'BufferOE',
		category: 'Basic',
		drawPreview: function (svg) {
			var layout = displayComponent.layout(['OE', ''], ['']);
			displayComponent(svg, layout.width, layout.height, layout.pins, '1');
		}
	};

	return BufferComponentOE;
});
