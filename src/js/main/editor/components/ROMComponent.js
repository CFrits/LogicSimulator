/**
 * Copyright: (c) 2019 Frits Wiersma
 * License: MIT
 */

define([
	'editor/Component',
	'editor/ComponentProperties',
	'editor/displayComponent',
	'shared/lib/extend'
], function (Component, ComponentProperties, displayComponent, extend) {
	var COMPONENT_LABEL = 'ROM';
	var COMPONENT_WIDTH = 11;

	function ROMComponent() {
		Component.call(this);

		this.pins = null;

		this.width = 0;
		this.height = 0;
		this.pins = null;

		this.$container = null;
		this.$rect = null;

		var self = this;
		function updateLayout() {
			self.layout();
			self._updateDisplay();
		}

		this.properties = new ComponentProperties([
			[ 'addresswidth', 'Address width', 'int', 4, updateLayout, { min: 1 } ],
			[ 'datawidth', 'Data width', 'int', 8, updateLayout, { min: 1, max: 32 } ],
			[ 'content', 'Data content ', 'string', '[ "0x01", "0x02", "0x03", "0x04", "0x05", "0x06", "0x07", "0x08" ]', updateLayout ]
		]);

		this.layout();
	}

	extend(ROMComponent, Component);

	ROMComponent.prototype._save = function (data) {
		data.addresswidth = this.properties.get('addresswidth');
		data.datawidth = this.properties.get('datawidth');
		data.content = this.properties.get('content');
	};

	ROMComponent.prototype._load = function (data) {
		this.properties.set('addresswidth', data.addresswidth);
		this.properties.set('datawidth', data.datawidth);
		this.properties.set('content', data.content);

		this.layout();
	};

	ROMComponent.prototype.layout = function () {
		var addresswidth = Math.max(1, this.properties.get('addresswidth'));
		var datawidth = Math.max(1, this.properties.get('datawidth'));
		var content   = this.properties.get('content');

		var inputCount = 3 + addresswidth + datawidth;
		var outputCount = datawidth;

		var inputs = ['OE', null];

		for (var i = 0; i < addresswidth; i++) {
			inputs.push('A' + i);
		}

		inputs.push(null);

		var outputs = [null];

		for (var i = 0; i < datawidth; i++) {
			outputs.push('Q' + i);
		}

		var layout = displayComponent.layout(inputs, outputs, COMPONENT_WIDTH);
		this.width = layout.width;
		this.height = layout.height;
		this.pins = layout.pins;
	};

	ROMComponent.prototype._display = function ($c) {
		this.$container = $c;
		this._updateDisplay();
	};

	ROMComponent.prototype._updateDisplay = function () {
		this.$container.innerHTML = '';
		this.$rect = displayComponent(this.$container, this.width, this.height, this.pins, COMPONENT_LABEL);
		this.$rect.addEventListener('mousedown', this.mousedownCallback);

		if(this.selected) {
			this._select();
		}
	};

	ROMComponent.prototype._select = function () {
		this.$rect.setAttribute('stroke', '#0288d1');
	};

	ROMComponent.prototype._deselect = function () {
		this.$rect.setAttribute('stroke', '#000');
	};

	ROMComponent.prototype._serializeForSimulation = function () {
		return {
			name: 'rom',
			args: [
				this.properties.get('addresswidth'),
				this.properties.get('datawidth'),
				this.properties.get('content')
			]
		};
	};

	ROMComponent.typeName = 'rom';
	ROMComponent.sidebarEntry = {
		name: 'ROM',
		category: 'Memory',
		drawPreview: function (svg) {
			var layout = displayComponent.layout(
				['OE', 'WE', null, 'A0', 'A1', 'A2', 'A3', null],
				['Q0', 'Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7'],
				COMPONENT_WIDTH
			);
			displayComponent(svg, layout.width, layout.height, layout.pins, COMPONENT_LABEL);
		}
	};

	return ROMComponent;
});
