/**
 * Copyright: (c) 2019 Frits Wiersma
 * License: MIT
 */

define([
    'editor/Component',
    'editor/ComponentProperties',
    'shared/lib/extend',
    'lib/SvgUtil'
], function (Component, ComponentProperties, extend, SvgUtil) {
    var WIDTH  = 5;
    var HEIGHT = 8;
    var NRPINS = 4;
    var NRSEGMENTS = 7;

    var DEFAULT_OFF_COLOR = '#888';
    var DEFAULT_ON_COLOR = '#e00';

    function layoutSevenSegmentComponent() {
        var height = HEIGHT;

        var pins = [];

        for (var i = 0; i < NRPINS; i++) {
            pins.push({
                out: false,
                x: -1,
                y: i * 2 + 1,
                inverted: false,
                label: '',
                index: i
            });
        }

        return {
            height: height,
            pins: pins
        };
    }

    function displaySevenSegmentComponent($container, color) {

        var $rect = SvgUtil.createElement('rect');
        $rect.setAttribute('width', WIDTH * 10);
        $rect.setAttribute('height', HEIGHT * 10);
        $rect.setAttribute('fill', 'grey');
        $rect.setAttribute('stroke', 'black');
        $rect.setAttribute('stroke-width', '2');
        $container.appendChild($rect);

		// Create the pins.
        for (var i = 0; i < NRPINS; i++) {
            var y = i * 2 + 1;
            var $pin = SvgUtil.createElement('line');
            $pin.setAttribute('x1', -1 * 10);
            $pin.setAttribute('y1', y * 10);
            $pin.setAttribute('x2', 0 * 10);
            $pin.setAttribute('y2', y * 10);
            $pin.setAttribute('stroke', 'black');
            $pin.setAttribute('stroke-width', '2');
            $container.appendChild($pin);
        }

        var $lights = [];

    	var SEGMENTS_WIDTH = WIDTH * 10 - 20;
    	var SEGMENTS_HIGHT = 6;
    	var SEGMENTS_PADDING = 6;

        // Segment A
        var $segmentA = SvgUtil.createElement('rect');
        $segmentA.setAttribute('x', 10);
        $segmentA.setAttribute('y', SEGMENTS_PADDING);
        $segmentA.setAttribute('width', SEGMENTS_WIDTH);
        $segmentA.setAttribute('height', SEGMENTS_HIGHT);
        $segmentA.setAttribute('fill', color);
        $segmentA.setAttribute('pointer-events', 'none');
        $container.appendChild($segmentA);
        $lights.push($segmentA);

        // Segment B
        var $segmentB = SvgUtil.createElement('rect');
        $segmentB.setAttribute('x', (WIDTH * 10) - 10);
        $segmentB.setAttribute('y', 10);
        $segmentB.setAttribute('width', SEGMENTS_HIGHT);
        $segmentB.setAttribute('height', SEGMENTS_WIDTH - SEGMENTS_PADDING);
        $segmentB.setAttribute('fill', color);
        $segmentB.setAttribute('pointer-events', 'none');
        $container.appendChild($segmentB);
        $lights.push($segmentB);

        // Segment C
        var $segmentC = SvgUtil.createElement('rect');
        $segmentC.setAttribute('x', (WIDTH * 10) - 10);
        $segmentC.setAttribute('y', (HEIGHT * 10) / 2 + SEGMENTS_PADDING);
        $segmentC.setAttribute('width', SEGMENTS_HIGHT);
        $segmentC.setAttribute('height', SEGMENTS_WIDTH - SEGMENTS_PADDING);
        $segmentC.setAttribute('fill', color);
        $segmentC.setAttribute('pointer-events', 'none');
        $container.appendChild($segmentC);
        $lights.push($segmentC);

        // Segment D
        var $segmentD = SvgUtil.createElement('rect');
        $segmentD.setAttribute('x', 10);
        $segmentD.setAttribute('y', (HEIGHT * 10) - 10);
        $segmentD.setAttribute('width', SEGMENTS_WIDTH);
        $segmentD.setAttribute('height', SEGMENTS_HIGHT);
        $segmentD.setAttribute('fill', color);
        $segmentD.setAttribute('pointer-events', 'none');
        $container.appendChild($segmentD);
        $lights.push($segmentD);

        // Segment E
        var $segmentE = SvgUtil.createElement('rect');
        $segmentE.setAttribute('x', 5);
        $segmentE.setAttribute('y', (HEIGHT * 10) / 2 + SEGMENTS_PADDING);
        $segmentE.setAttribute('width', SEGMENTS_HIGHT);
        $segmentE.setAttribute('height', SEGMENTS_WIDTH - SEGMENTS_PADDING);
        $segmentE.setAttribute('fill', color);
        $segmentE.setAttribute('pointer-events', 'none');
        $container.appendChild($segmentE);
        $lights.push($segmentE);

        // Segment F
        var $segmentF = SvgUtil.createElement('rect');
        $segmentF.setAttribute('x', 5);
        $segmentF.setAttribute('y', 10);
        $segmentF.setAttribute('width', SEGMENTS_HIGHT);
        $segmentF.setAttribute('height', SEGMENTS_WIDTH - SEGMENTS_PADDING);
        $segmentF.setAttribute('fill', color);
        $segmentF.setAttribute('pointer-events', 'none');
        $container.appendChild($segmentF);
        $lights.push($segmentF);

        // Segment G
        var $segmentG = SvgUtil.createElement('rect');
        $segmentG.setAttribute('x', 10);
        $segmentG.setAttribute('y', (HEIGHT * 10) / 2 - SEGMENTS_PADDING / 2);
        $segmentG.setAttribute('width', SEGMENTS_WIDTH);
        $segmentG.setAttribute('height', SEGMENTS_HIGHT);
        $segmentG.setAttribute('fill', color);
        $segmentG.setAttribute('pointer-events', 'none');
        $container.appendChild($segmentG);
        $lights.push($segmentG);

        return {
            $rect: $rect,
            $lights: $lights
        };
    }

    function SevenSegmentComponent() {
        Component.call(this);

        this.width = WIDTH;
        this.height = HEIGHT;
        this.pins = null;

        this.$container = null;
        this.$rect = null;
        this.$lights = null;

        this.segments = NRSEGMENTS;
        // Number of pins is fixed to 4 to decode all 7 segments.
        this.size = NRPINS;
        this.offColor = null;
        this.onColor = null;

        var self = this;

        function updateDisplay() {
            self.layout();
            self._updateDisplay();
        }

        this.properties = new ComponentProperties([
            [ 'off-color', 'Color (Off)', 'string', DEFAULT_OFF_COLOR, updateDisplay ],
            [ 'on-color', 'Color (On)', 'string', DEFAULT_ON_COLOR, updateDisplay ]
        ]);

        this.layout();
    }

    extend(SevenSegmentComponent, Component);

    SevenSegmentComponent.prototype._save = function (data) {
        data.offColor = this.properties.get('off-color');
        data.onColor = this.properties.get('on-color');
    };

    SevenSegmentComponent.prototype._load = function (data) {
        this.properties.set('off-color', data.offColor);
        this.properties.set('on-color', data.onColor);

        this.layout();
    };

    SevenSegmentComponent.prototype.layout = function () {
        this.offColor = this.properties.get('off-color');
        this.onColor = this.properties.get('on-color');

        var layout = layoutSevenSegmentComponent();
        this.height = layout.height;
        this.pins = layout.pins;
    };

    SevenSegmentComponent.prototype._display = function ($c) {
        this.$container = $c;
        this._updateDisplay();
    };

    SevenSegmentComponent.prototype._updateDisplay = function () {
        if(!this.$container) return;

        this.$container.innerHTML = '';
        var elements = displaySevenSegmentComponent(this.$container, this.offColor);
        this.$lights = elements.$lights;
        this.$rect = elements.$rect;
        this.$rect.addEventListener('mousedown', this.mousedownCallback);

        if(this.selected) {
            this._select();
        }
    };

    SevenSegmentComponent.prototype._select = function () {
        this.$rect.setAttribute('stroke', '#0288d1');
    };

    SevenSegmentComponent.prototype._deselect = function () {
        this.$rect.setAttribute('stroke', '#000');
    };

    SevenSegmentComponent.prototype._serializeForSimulation = function () {
        return {
            name: 'sevensegment',
            args: [ this.size ]
        };
    };

    SevenSegmentComponent.prototype.updateSimulationDisplay = function (displayData) {

		var segments0 = [ 0, 1, 2, 3, 4, 5 ];
		var segments1 = [ 1, 2 ];
		var segments2 = [ 0, 1, 3, 4, 6 ];
		var segments3 = [ 0, 1, 2, 3, 6 ];
		var segments4 = [ 1, 2, 5, 6 ];
		var segments5 = [ 0, 2, 3, 5, 6 ];
		var segments6 = [ 2, 3, 4, 5, 6 ];
		var segments7 = [ 0, 1, 2 ];
		var segments8 = [ 0, 1, 2, 3, 4, 5, 6 ];
		var segments9 = [ 0, 1, 2, 5, 6 ];
		var segmentsA = [ 0, 1, 2, 4, 5, 6 ];
		var segmentsB = [ 2, 3, 4, 5, 6 ];
		var segmentsC = [ 0, 3, 4, 5 ];
		var segmentsD = [ 1, 2, 3, 4, 6 ];
		var segmentsE = [ 0, 3, 4, 5, 6 ];
		var segmentsF = [ 0, 4, 5, 6 ];

		var segmentson = [ segments0, segments1, segments2, segments3, segments4, segments5, segments6, segments7,
		                   segments8, segments9, segmentsA, segmentsB, segmentsC, segmentsD, segmentsE, segmentsF ];

		// Turn all segments off.
        for (var i = 0; i < this.segments; i++) {
            this.$lights[i].setAttribute('fill', this.offColor);
        }

		// Turn on the segments corresponding the the digit to display.
        for (var i = 0; i < segmentson[displayData].length; i++) {
			var s = segmentson[displayData][i]
            this.$lights[s].setAttribute('fill', this.onColor);
        }

    };

    SevenSegmentComponent.prototype.resetSimulationDisplay = function () {
        for (var i = 0; i < this.segments; i++) {
            this.$lights[i].setAttribute('fill', this.offColor);
        }
    };

    SevenSegmentComponent.typeName = 'sevensegment';
    SevenSegmentComponent.sidebarEntry = {
        name: 'Seven Segment',
        category: 'Input/Output',
        drawPreview: function (svg) {
            displaySevenSegmentComponent(svg, DEFAULT_ON_COLOR);
        }
    };

    return SevenSegmentComponent;
});
