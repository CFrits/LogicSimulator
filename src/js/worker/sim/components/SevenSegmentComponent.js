/**
 * Copyright: (c) 2019 Frits Wiersma
 * License: MIT
 */

define([
	'sim/Component',
	'ComponentRegistry',
	'shared/lib/createArray',
	'shared/lib/extend'
], function (Component, ComponentRegistry, createArray, extend) {
	function SevenSegmentComponent(args) {
		Component.call(this, args);

		this.in = createArray(args[0], false);
		this.out = [];
	}

	extend(SevenSegmentComponent, Component);

	SevenSegmentComponent.prototype.getDisplayData = function () {
		return this.in[3] * 8 + this.in[2] * 4 + this.in[1] * 2 + this.in[0];
	};

	SevenSegmentComponent.prototype.exec = function () {};

	ComponentRegistry.register('sevensegment', SevenSegmentComponent);

	return SevenSegmentComponent;
});
