/**
 * Copyright: (c) 2019 Frits Wiersma
 * License: MIT
 */

define([
	'sim/Component',
	'ComponentRegistry',
	'shared/lib/extend'
], function (Component, ComponentRegistry, extend) {
	function BufferComponentOE() {
		Component.call(this);

		this.in = [false, false];
		this.out = [false];
	}

	extend(BufferComponentOE, Component);

	BufferComponentOE.prototype.exec = function () {
        var oe = this.in[0];

        if (oe) {
		    this.out[0] = this.in[1];
        } else {
		    this.out[0] = false;
        }
	};

	ComponentRegistry.register('bufferOE', BufferComponentOE);

	return BufferComponentOE;
});
