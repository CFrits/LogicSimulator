/**
 * Copyright: (c) 2019 Frits Wiersma
 * License: MIT
 */

define([
	'sim/Component',
	'ComponentRegistry',
	'shared/lib/createArray',
	'shared/lib/extend'
], function (Component, ComponentRegistry, createArray, extend) {
	function ROMComponent(args) {
		Component.call(this, args);

		this.addresswidth = args[0];
		this.datawidth = args[1];
		this.content = args[2];

		this.size = Math.pow(2, args[0]);
		this.data = createArray(this.size, 0);
		this.data = JSON.parse(this.content);

		this.in = createArray(1 + args[0] + args[1], false);
		this.out = createArray(args[1], false);

		this.lastOutputData = 0;
	}

	extend(ROMComponent, Component);

	ROMComponent.prototype.exec = function () {
		var oe = this.in[0];

		var outputData = 0;

		if (oe) {
			var address = 0;

			for (var i = 0; i < this.addresswidth; i++) {
                // Read the address from A0 ...
				if (this.in[1 + i]) {
					address |= 1 << i;
				}
			}

		    outputData = this.data[address];
		}

		if (outputData !== this.lastOutputData) {
			for (var i = 0; i < this.datawidth; i++) {
				this.out[i] = !!(outputData & (1 << i));
			}
		}

		this.lastOutputData = outputData;
	};

	ComponentRegistry.register('rom', ROMComponent);

	return ROMComponent;
});
